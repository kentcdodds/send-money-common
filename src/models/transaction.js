
export default createTransaction;

if (process.env.NODE_ENV !== 'production') {
  createTransaction.mock = require('./transaction.mock');
}


function createTransaction(transaction) {
  // this is where I'd add defaults. For this basic case
  // we're going to just give them back what they gave us.
  return {
    ...transaction
  };
}
