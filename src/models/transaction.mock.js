import faker from 'faker';
import _ from 'lodash';

export default mockTransaction;

function mockTransaction(overrides) {
  const business = Math.random() >= 0.7;
  let amount = Math.round(_.random(10, 1000, true) * 100) / 100;
  if (Math.random() > 0.5) {
    amount = -amount;
  }
  return {
    email: faker.internet.email(),
    message: faker.lorem.sentences(2),
    currencyType: _.sample(['USD', 'EUR', 'JPY']),
    purpose: business ? 'GOODS_OR_SERVICES' : 'FAMILY_OR_FRIEND',
    amount,
    recipient: business ? faker.company.companyName() : faker.name.findName(),
    date: faker.date.past(1).toISOString(),
    _id: faker.random.uuid(),
    ...overrides
  };
}
